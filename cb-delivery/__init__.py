import shutil
import uuid
from base import GitClone
import sys
import os
import re
import configparser
import logging
import base64
from datetime import datetime
import uuid

from constants import *

# retrieve config values
config = configparser.ConfigParser()
config.read('config.ini')
config_check = config['check']
config_dir = config['dir']
config_couchbase_variables = config['couchbase_variables']
config_url_log = config['url']
ext_file = ['.yml', '.yaml']
folder_common_config = 'configurations'

# logging setting
id_process = datetime.now().strftime("%Y%m%d%H%M%S") + '_' + str(uuid.uuid1())
log_file_name = "{0}/{1}.log".format(config_dir['logging_path'], 'cb_delivery_' + id_process)

# tmp folder
tmp_path = config_dir['tmp_path']
folder_common = os.path.join(tmp_path, 'cb_delivery_src_' + '_' + id_process)
folder_target = os.path.join(tmp_path, 'cb_delivery_target_' + '_' + id_process)

#For local debug
#folder_target = "C:/Users/Utente/Documents/Bitbucket/intesa.script.cb-automation/yaml-output/"
class ResponseScript(object):
    def __str__(self):
        return str(self.__dict__) + '\n'

    def __init__(self, message, hpoo, region_ocp, url):
        self.message = message
        self.HPOO = hpoo
        self.REGION_OCP = region_ocp
        self.URL = url


def main(argv):
    actor_inputs = {}
    for e in argv:
        key, value = e.split(':')
        key_upper = key.upper()
        actor_inputs[key_upper] = value.lower()

    try:
        nome_servizio = actor_inputs.get('SERVIZIO_DB_GSS')
        modalita = actor_inputs.get(MODALITA_KEY)

        validate_required(actor_inputs, modalita)

        validate_isp_value(actor_inputs)

        if int(modalita) == 1:
            calculate_size_couchbase(actor_inputs)

        common_repo = GitClone('isp-db/cb-master', folder_common, 'main')
        common_repo.close()

        # build target path
        target = 'cb-{0}-{1}'.format(actor_inputs.get(ENV_KEY), actor_inputs.get(REGION_KEY))
        
        # build the dictionary from the config file on common repo
        config_dict = build_config_dict(target)
        # merge two dictionary
        dict_tot = {**config_dict, **actor_inputs}

        # For local debug
        #dict_tot = { **actor_inputs}
        
        # logging.info('Varibales map: '+ str(dict_tot))
        logger('Varibales map: ' + str(dict_tot))
        # clone <ACRO> in folder_target dir

        nome_repo = 'cb-common'
        if int(modalita) == 0:
            nome_repo = 'cb-common'
            url = ''
        else:
            nome_repo = 'cb-' + actor_inputs.get('SERVIZIO_DB_GSS')

            url = config_url_log['structure_log_url']\
                    .replace('<NOME_SERVIZIO>', nome_servizio)\
                    .replace('<ACRO>', actor_inputs.get(ACRO_KEY))\
                    .replace('<AMBIENTE>', actor_inputs.get(ENV_KEY))\
                    .replace('<CLUSTER>', actor_inputs.get(REGION_KEY))
        
        try:
            target_repo = GitClone(actor_inputs.get(ACRO_KEY) + '-cfg/' + nome_repo, folder_target, actor_inputs.get(ENV_KEY))
        except Exception as e:
            if e.__str__().__contains__('Repository not found'):
                message = "Non risulta alcun repository " + actor_inputs.get(ENV_KEY)
                raise Exception(message)

            matches = ["Remote branch", "not found"]

            if any(word in e.__str__() for word in matches):
                message = "Non risulta alcun branch " + actor_inputs.get(ENV_KEY) + " nel repository " + \
                          str(actor_inputs.get(ACRO_KEY)) + "-cfg/" + str(nome_repo)
                raise Exception(message)
            else:
                raise Exception(e)
        
        try:
            os.mkdir(folder_target + '/' + actor_inputs.get(REGION_KEY))
        except Exception as e:
            message = "Il branch " + actor_inputs.get(ENV_KEY) + " del repository " + \
                      str(actor_inputs.get(ACRO_KEY)) + "-cfg/" + str(nome_repo) + \
                      " risulta già elaborato per la specifica istanza richiesta"
            raise Exception(message)
        

        # solve ph for all yaml files
        replace_ph(modalita, dict_tot, '/' + actor_inputs.get(REGION_KEY))
        # add, commit, push
        target_repo.push_branch()
        target_repo.close()

        return ResponseScript('Operazione Completata', 'OK', 'primary', url)  # URL = <NOME_SERVIZIO>-cb-<ACRO>-<AMBIENTE>.apps.<CLUSTER>.intesasanpaolo.com
    except Exception as e:
        # logging.info('An error occurred: ' + str(e))
        logger('An error occurred: ' + str(e))
        return ResponseScript(str(e), 'KO', '', '')
    # remove tmp folder
    finally:
        if os.path.exists(folder_common):
            shutil.rmtree(folder_common)
        if os.path.exists(folder_target):
            shutil.rmtree(folder_target)



def calculate_size_couchbase(actor_inputs):
    # calcolo logiche di determinati valori di Couchbase in base ai dati in input
    documents_num = int(actor_inputs.get(NUM_DOC_KEY))  # Numero documenti
    ID_size = int(actor_inputs.get(ID_SIZE_KEY))  # Dimensione ID Documento
    value_size = int(actor_inputs.get(VALUE_SIZE_KEY))  # Dimensione dati del documento
    working_set_percentage = int(actor_inputs.get(WORKING_SET_PERC_KEY)) / 100  # Percentuale di dati “caldi”
    access_type = actor_inputs.get(ACCESS_TYPE_KEY).upper()  # Tipo di accesso (KV-WRITE-READ)
    valore_flag_criticita = int(actor_inputs.get(FLAG_CRITIC_KEY))
    number_of_replicas = int(actor_inputs.get(NUM_REPLICA_KEY)) + valore_flag_criticita  # Flag “ALTAMENTE CRITICO” (1 - 2)

    number_of_nodes = int(config_couchbase_variables['number_of_nodes'])
    high_water_mark = int(config_couchbase_variables['high_water_mark_percentage']) / 100
    headroom = int(config_couchbase_variables['headroom_percentage']) / 100
    metadata_per_document = int(config_couchbase_variables['metadata_per_document'])
    min_RAM_per_POD_data_Service = int(config_couchbase_variables['min_byte_RAM_per_POD_data_Service'])  # (Min 2048 Mb)
    min_RAM_per_POD_index_Service = int(config_couchbase_variables['min_byte_RAM_per_POD_index_Service'])  # (Min 512Mb)
    max_RAM_per_POD_data_Service = int(config_couchbase_variables['max_byte_RAM_per_POD_data_Service'])  # (Max 16 Gb)
    max_RAM_per_POD_index_Service = int(config_couchbase_variables['max_byte_RAM_per_POD_index_Service'])  # (Max 16 Gb)

    access_type_read_value = int(config_couchbase_variables['access_type_read_value'])
    access_type_write_value = int(config_couchbase_variables['access_type_write_value'])
    access_type_kv_value = int(config_couchbase_variables['access_type_kv_value'])

    no_of_copies = 1 + number_of_replicas
    total_metadata = round(documents_num * (metadata_per_document + ID_size) * no_of_copies)
    total_dataset = round(documents_num * value_size * no_of_copies)
    working_set = round(total_dataset * working_set_percentage)
    cluster_RAM_quota_required_for_Data = round((total_metadata + working_set) * (1 + headroom) / high_water_mark)
    RAM_per_POD_data_Service = round(cluster_RAM_quota_required_for_Data / number_of_nodes)

    if RAM_per_POD_data_Service < min_RAM_per_POD_data_Service:
        RAM_per_POD_data_Service = min_RAM_per_POD_data_Service
    if RAM_per_POD_data_Service > max_RAM_per_POD_data_Service:
        RAM_per_POD_data_Service = max_RAM_per_POD_data_Service

    if 'READ'.__eq__(access_type.upper()):
        number_of_index_fields = access_type_read_value
    else:
        if 'WRITE'.__eq__(access_type.upper()):
            number_of_index_fields = access_type_write_value
        else:
            number_of_index_fields = access_type_kv_value

    secondary_index_size = round(ID_size * 2)
    index_size = round(documents_num * (ID_size + (secondary_index_size * number_of_index_fields)))
    RAM_per_POD_index_Service = round(index_size * working_set_percentage * (1 + headroom) / high_water_mark)
    if RAM_per_POD_index_Service < min_RAM_per_POD_index_Service:
        RAM_per_POD_index_Service = min_RAM_per_POD_index_Service
    if RAM_per_POD_index_Service > max_RAM_per_POD_index_Service:
        RAM_per_POD_index_Service = max_RAM_per_POD_index_Service

    somma_RAM_totale_POD = round(RAM_per_POD_data_Service + RAM_per_POD_index_Service + 2 * pow(1024, 3))
    actor_inputs[RAM_TOT_POD_KEY] = str(somma_RAM_totale_POD)
    data_fs = round((total_metadata + total_dataset) * 2 / number_of_nodes)  # (2 or 3 times the size of the data)
    actor_inputs[DATA_FS_KEY] = str(data_fs)
    index_fs = round(index_size * 2)  # (2 or 3 times the size of the data)
    actor_inputs[INDEX_FS_KEY] = str(index_fs)
    actor_inputs['BUCKET_QUOTA'] = str(
        round((RAM_per_POD_data_Service * int(config_couchbase_variables['percentage_bucket_quota'])) / 100))
    actor_inputs['RAM_PER_POD_DATA_SERVICE'] = str(RAM_per_POD_data_Service)
    actor_inputs['RAM_PER_POD_INDEX_SERVICE'] = str(RAM_per_POD_index_Service)


def replace_value_env(inputs):
    # controllo valorizzazione variabile ENV
    valid_domain_env = eval(config_check['acceptable_values_env'])

    env_value = inputs.get(ENV_KEY)
    for key, value in valid_domain_env.items():
        if env_value == key.lower():
            inputs[ENV_KEY] = value.lower()
            inputs[ENV_KEY + '_UP'] = value.upper()
            return

    raise Exception('Il valore passato in input nel campo ENV non è valido. [' + env_value + ']')

def validate_isp_value(actor_inputs): 
    """
    Se non esiste un ISP_VALUE passato
    come parametro, aggiungo il valore
    di default prometheus
    """
    if("isp_value" in config['monitoring']):
        actor_inputs[ISP_VALUE] = config['monitoring']['isp_value']
    else: 
        raise Exception("Il valore isp_value delle variabili d'ambiente non è valido")



def validate_required(inputs, modalita):
    d_absent = []

    # check input modalita
    if int(inputs.get(MODALITA_KEY)) != 0 and int(inputs.get(MODALITA_KEY)) != 1:
        raise Exception(
            'Il valore passato in input nel campo MODALITA non è valido. [' + inputs.get(MODALITA_KEY) + ']')

    required_input_mode = 'required_input_mod_' + str(modalita)

    required = list(map(capitalize_list, config_check[required_input_mode].split(',')))
    for e in required:
        if not e in inputs.keys():
            d_absent.append(e)
    if len(d_absent) > 0:
        raise Exception('Missing the following mandatory input: ' + str(d_absent))

    # check input ENV and replace
    replace_value_env(inputs)

    # check input number_of_documents
    if required.__contains__(NUM_DOC_KEY):
        check_values = config_check['required_values_number_of_documents'].split(',')
        if inputs.get(NUM_DOC_KEY) not in check_values:
            raise Exception('Il valore passato in input nel campo NUMBER_OF_DOCUMENTS non è valido. [' + inputs.get(
                NUM_DOC_KEY) + ']')
    # check input ID_SIZE
    if required.__contains__(ID_SIZE_KEY):
        check_values = config_check['required_values_id_size'].split(',')
        if inputs.get(ID_SIZE_KEY) not in check_values:
            raise Exception(
                'Il valore passato in input nel campo ID_SIZE non è valido. [' + inputs.get(ID_SIZE_KEY) + ']')
    # check input VALUE_SIZE
    if required.__contains__(VALUE_SIZE_KEY):
        max_byte_dimensione_dati_documento = int(config_check.get('max_byte_dimensione_dati_documento'))  # 100 Kilobyte
        if int(inputs.get(VALUE_SIZE_KEY)) > max_byte_dimensione_dati_documento:
            raise Exception(
                'Il valore passato in input nel campo VALUE_SIZE non è valido. [' + inputs.get(VALUE_SIZE_KEY) + ']')
    # check input WORKING_SET_PERCENTAGE
    if required.__contains__(WORKING_SET_PERC_KEY):
        check_values = config_check['required_values_working_set_percentage'].split(',')
        if inputs.get(WORKING_SET_PERC_KEY) not in check_values:
            raise Exception('Il valore passato in input nel campo WORKING_SET_PERCENTAGE non è valido. [' + inputs.get(
                WORKING_SET_PERC_KEY) + ']')
    # check input tipo di accesso
    if required.__contains__(ACCESS_TYPE_KEY):
        check_values = config_check['required_values_access_type'].split(',')
        if inputs.get(ACCESS_TYPE_KEY).upper() not in check_values:
            raise Exception(
                'Il valore passato in input nel campo ACCESS_TYPE non è valido. [' + inputs.get(ACCESS_TYPE_KEY) + ']')
    # check input flag criticita
    if required.__contains__(FLAG_CRITIC_KEY):
        if int(inputs.get(FLAG_CRITIC_KEY)) != 0 and int(inputs.get(FLAG_CRITIC_KEY)) != 1:
            raise Exception('Il valore passato in input nel campo FLAG_CRITICITA non è valido. [' + inputs.get(
                FLAG_CRITIC_KEY) + ']')


def capitalize_list(item):
    return item.upper()


def replace_ph(modalita, dict, path_yaml_da_creare):

    if int(modalita) == 0:
        folder_common = os.path.join(tmp_path, 'cb_delivery_src_' + '_' + id_process + '/acro-yaml')
    else:
        folder_common = os.path.join(tmp_path, 'cb_delivery_src_' + '_' + id_process + '/svc-yaml')

    #AsoStrife
    folder_common = "C:/Users/Utente/Documents/Bitbucket/intesa.script.cb-automation/yaml-input/"

    for file_to_commit in os.listdir(folder_common):
        if os.path.splitext(file_to_commit)[1] in ext_file:
            with open(os.path.join(folder_common, file_to_commit), 'r') as instream:
                content = instream.read()
                content = re.sub("\${(.*?)}", replace_var(dict), content)
                content = re.sub("\$\[base64:(.*?)]", replace_base64(), content)
                outF = open(os.path.join(folder_target + path_yaml_da_creare, file_to_commit), "w")
                outF.write(content)
                outF.close()

    path_sottocartella_yaml_aggiuntivi = os.path.join(
        folder_common + '/cb-{0}-{1}'.format(dict.get(ENV_KEY), dict.get(REGION_KEY)))

    if os.path.exists(path_sottocartella_yaml_aggiuntivi):
        for file_to_commit in os.listdir(path_sottocartella_yaml_aggiuntivi):
            if os.path.splitext(file_to_commit)[1] in ext_file:
                with open(os.path.join(path_sottocartella_yaml_aggiuntivi, file_to_commit), 'r') as instream:
                    content = instream.read()
                    content = re.sub("\${(.*?)}", replace_var(dict), content)
                    content = re.sub("\$\[base64:(.*?)]", replace_base64(), content)
                    outF = open(os.path.join(folder_target + path_yaml_da_creare, file_to_commit), "w")
                    outF.write(content)
                    outF.close()


def build_config_dict(target):
    if not os.path.exists(os.path.join(folder_common, folder_common_config, target + '.yaml')):
        raise Exception('Il path [' + os.path.join(folder_common, folder_common_config,
                                                   target + '.yaml') + ' ] non corrisponde. Cartella o file non esistenti.')

    d = {}
    with open(os.path.join(folder_common, folder_common_config, target + '.yaml')) as f:
        for line in f:
            (key, val) = line.split(':', 1)  # TODO: usare un parser
            d[(key.strip())] = val.strip()
    return d


def replace_var(values):
    def lookup(match):
        key = match.group(1)
        # TODO throw exception
        return values.get(key, f'<{key} not found>')

    return lookup


def replace_base64():
    def lookup(match):
        key = match.group(1)
        return base64.b64encode(key.encode('UTF-8')).decode('UTF-8')

    return lookup


def logger(message="", v=False):
    """
    message: str
    v: boolean, optional

    Saves the message passed as a parameter in the
    log file defined on log_file_name. If v == true
    the message is also printed in the terminal
    """
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    # Sanity check
    if isinstance(message, str) == False:
        message = str(message)

    log_text = now + "\t" + message + "\n"

    # Save log file
    with open(log_file_name, 'a+') as f:
        f.write(log_text)

    if v == True:
        print(message)


if __name__ == "__main__":
    try:
        response = main(sys.argv[1:])
        logger(str(response), True)
    except Exception as e:
        logging.exception('An error occurred:')