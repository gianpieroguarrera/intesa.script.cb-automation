import configparser
import git

config = configparser.ConfigParser()
config.read('config.ini')
git_config = config['repository']


class GitClone:

    def __init__(self, repo_name, repo_dir, from_branch):
        self.repo_user = git_config['user']
        self.repo_pass = git_config['password']
        self.repo_name = repo_name
        repo_url = '{0}://{1}:{2}@{3}{4}.git'
        self.repo_url = repo_url.format(git_config['protocol'], self.repo_user, self.repo_pass,
                                        git_config['common_url'], self.repo_name)
        self.repo_dir = repo_dir or self.repo_name
        self.from_branch = from_branch
        self.branch = self.from_branch
        self.repo = git.Repo.clone_from(self.repo_url, self.repo_dir, branch=self.from_branch)

    def close(self):
        self.repo.close()

    def push_branch(self):
        self.repo.git.add(all=True)
        self.repo.index.commit('first commit')
        origin = self.repo.remote(name='origin')
        self.repo.git.push("--set-upstream", origin, self.repo.head.ref)
        origin.push()

    def create_branch(self, param):
        self.repo.git.checkout('master')
        self.repo.git.checkout(b=param)
