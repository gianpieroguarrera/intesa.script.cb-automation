import requests
import re
import sys
import json
import os
import subprocess


# ./cb-backup.py <type> <url> <user> <pass> <repo_folder> <external> <n_backup>


def print_usage():
    print("./cb-backup.py <type> <url> <user> <pass> <repo_name> <repo_folder> <external> [n_backup]")


if (len(sys.argv) < 8):
    print("Mancano dei parametri")
    print_usage()
    exit(-1)

TYPE = sys.argv[1]
URL = sys.argv[2]
USER = sys.argv[3]
PASS = sys.argv[4]
REPO_NAME = sys.argv[5]
REPO_FOLDER = sys.argv[6]
EXTERNAL = sys.argv[7]

TYPE = TYPE.upper()

if not (re.match(r"^[C|B|M]", TYPE)):
    print("Manca parametro TYPE o TYPE errato")
    print("C: Configure")
    print("B: Backup")
    print("M: Merge")
    print_usage()
    exit(-2)

if not (re.match(r"^HTTP.*:[0-9]*[^/]$", URL.upper())):
    print("Url errato, deve essere in formatto http://host:port senza / finale")
    print_usage()
    exit(-4)

if not (re.match(r"^/", REPO_FOLDER.upper())):
    print("Folder repository errato, deve essere in formato assoluto")
    print_usage()
    exit(-8)

if not (re.match(r"^[Y|N]$", EXTERNAL.upper())):
    print("External deve essere Y o N")
    print_usage()
    exit(-16)

if (TYPE == "M"):
    if (len(sys.argv) < 9):
        print("Mancano dei parametri")
        print_usage()
        exit(-1)
    else:
        N_BACKUP = sys.argv[8]
        if not (re.match(r"^[0-9]*$", N_BACKUP.upper())):
            print("n_backup deve essere un numero")
            print_usage()
            exit(-16)

# GLOBAL
CBBACKUPMGR = '/opt/couchbase/bin/cbbackupmgr'


def get_external_net():
    print("Recupero connettivita' esterna")
    pool = requests.get(URL + "/pools/default", auth=(USER, PASS))
    if (pool.status_code == 200):
        # print(pool.json()['nodes'][0]['alternateAddresses']['external'])
        external_json = pool.json()['nodes'][0]['alternateAddresses']['external']
        return ('couchbase://' + external_json['hostname'] + ':' + str(external_json['ports']['mgmt']))
    else:
        print("errore estrazione pool da couchbase:" + str(pool.status_code))
        if (pool.status_code == 401):
            print("401 = Errore di autenticazione, controllare utente e password")
        exit(-32)


def backup():
    print("Avvio procedura di backup")
    print(EXTERNAL.upper())
    if (EXTERNAL.upper() == 'Y'):
        cluster = get_external_net()
    else:
        cluster = URL
    print("Eseguo backup da: " + cluster)
    # /opt/couchbase/bin/cbbackupmgr backup -c cluster -u user -p pass -a folder -r repo
    comm = CBBACKUPMGR + ' backup -c ' + cluster + ' -u ' + USER + ' -p ' + PASS + ' -a ' + REPO_FOLDER + ' -r ' + REPO_NAME + " --purge --no-progress-bar"
    print("avvio comando: " + comm)
    os.system(comm)


def configure():
    print("Avvio procedura di configurazione del repositoy: " + REPO_NAME)
    print("nel folder: " + REPO_FOLDER)
    comm = CBBACKUPMGR + ' config -a ' + REPO_FOLDER + ' -r ' + REPO_NAME
    print("avvio comando: " + comm)
    os.system(comm)


def get_bck_date(b):
    return b['date']


def merge():
    print("avvio procedura di merge sul repository: " + REPO_NAME)
    print("nel folder: " + REPO_FOLDER)
    print("Veranno mantenuti " + N_BACKUP + " Backup totali")
    comm = CBBACKUPMGR + ' info -a ' + REPO_FOLDER + ' -r ' + REPO_NAME + " --json"
    print("avvio comando: " + comm)
    bck = subprocess.check_output(comm, shell=True)
    bck_json = json.loads(bck)
    if len(bck_json['backups']) <= int(N_BACKUP):
        print('Sono presenti ' + str(len(bck_json['backups'])) + " backups. Devo conservarne " + str(
            N_BACKUP) + ". Esecuzione terminata")
        exit(0)
    ##faccio sort dei backup nel dubbio
    # bcks=bck_json['backups']
    # bcks.sort(key=get_bck_date)
    # print(bcks)
    # bck_start=bcks[0]['date']
    # bck_end=bcks[-int(N_BACKUP)]['date']
    # print("Eseguo il merge da: "+bck_start+" a: "+bck_end)
    # comm=CBBACKUPMGR+' merge -a '+REPO_FOLDER+' -r '+REPO_NAME+" --start "+bck_start+" --end "+bck_end
    comm = CBBACKUPMGR + ' merge -a ' + REPO_FOLDER + ' -r ' + REPO_NAME + " --date-range 0," + str(
        len(bck_json['backups']) - int(N_BACKUP)) + " --no-progress-bar"
    print("avvio comando: " + comm)
    os.system(comm)


# MAIN

if TYPE == 'C':
    configure()
if TYPE == 'B':
    backup()
if TYPE == 'M':
    merge()
